package bigmap.magicboard;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import org.opencv.core.Mat;
import processing.core.PApplet;
import processing.core.PImage;

public class Conversion {

	public static PImage toPImage(PApplet applet, Mat m) {
		PImage img = new PImage();
		toPImage(applet, m, img);
		return img;
	}

	public static void toPImage(PApplet applet, Mat m, PImage img) {
		img.init(m.width(), m.height(), PApplet.ARGB);
		if (m.channels() == 3) {
			byte[] matPixels = new byte[m.width() * m.height() * 3];
			m.get(0, 0, matPixels);
			for (int i = 0; i < m.width() * m.height() * 3; i += 3) {
				img.pixels[i / 3] = applet.color(matPixels[i + 2] & 0xFF,
						matPixels[i + 1] & 0xFF, matPixels[i] & 0xFF);
			}
		} else if (m.channels() == 1) {
			byte[] matPixels = new byte[m.width() * m.height()];
			m.get(0, 0, matPixels);
			for (int i = 0; i < m.width() * m.height(); i++) {
				img.pixels[i] = applet.color(matPixels[i] & 0xFF);
			}
		} else if (m.channels() == 4) {
			byte[] matPixels = new byte[m.width() * m.height() * 4];
			m.get(0, 0, matPixels);
			for (int i = 0; i < m.width() * m.height() * 4; i += 4) {
				img.pixels[i / 4] = applet.color(matPixels[i + 2] & 0xFF,
						matPixels[i + 1] & 0xFF, matPixels[i] & 0xFF,
						matPixels[i + 3] & 0xFF);
			}
		}

		img.updatePixels();
	}

	public static Mat toCv(PImage img) {
		Mat m = new Mat();
		toCv(img, m);
		return m;
	}

	public static void toCv(PImage img, Mat m) {
		BufferedImage image = (BufferedImage) img.getNative();
		int[] matPixels = ((DataBufferInt) image.getRaster().getDataBuffer())
				.getData();

		ByteBuffer bb = ByteBuffer.allocate(matPixels.length * 4);
		IntBuffer ib = bb.asIntBuffer();
		ib.put(matPixels);

		byte[] bvals = bb.array();

		m.put(0, 0, bvals);
	}
}
