package bigmap.magicboard;

/*
 * Author: ATUL
 * This code can be used as an alternative to imshow of OpenCV for JAVA-OpenCv 
 * Make sure OpenCV Java is in your Build Path
 * Usage :
 * -------
 * Imshow ims = new Imshow("Title");
 * ims.showImage(Mat image);
 * Check Example for usage with Webcam Live Video Feed
 */

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

public class Imshow {

	public JFrame window;
	private ImageIcon image;
	private JLabel label;
	private MatOfByte matOfByte;
	private boolean sizeCustom;
	private int height, width, x, y;

	public Imshow(String title) {
		window = new JFrame();
		image = new ImageIcon();
		label = new JLabel();
		matOfByte = new MatOfByte();
		label.setIcon(image);
		window.getContentPane().add(label);
		window.setResizable(true);
		window.setTitle(title);
		sizeCustom = false;
		x = 0;
		y = 0;
	}

	public Imshow(String title, int width, int height) {
		this(title);
		sizeCustom = true;
		this.height = height;
		this.width = width;
	}
	
	public Imshow(String title, int width, int height, int x, int y) {
		this(title, width, height);
		this.x = x;
		this.y = y;
		window.setLocation(x, y);
	}

	public void showImage(Mat img) {
		img = img.clone();
		if (sizeCustom) {
			Imgproc.resize(img, img, new Size(width, height));
		}
		Highgui.imencode(".jpg", img, matOfByte);
		byte[] byteArray = matOfByte.toArray();
		BufferedImage bufImage = null;
		try {
			InputStream in = new ByteArrayInputStream(byteArray);
			bufImage = ImageIO.read(in);
			image.setImage(bufImage);
			window.pack();
			label.updateUI();
			window.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}