package bigmap.magicboard;

import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class ImageHelper {

	public static void detechMarker(Mat img) {
		
	}

	public static void drawMarker() {
		
	}

	public static void fit(Mat img, int width, int height) {
		double ratio = Math.min((double) width / img.cols(), (double) height
				/ img.rows());
		Imgproc.resize(img, img, new Size(0, 0), ratio, ratio,
				Imgproc.INTER_LANCZOS4);
		int xpad = width - img.cols();
		int ypad = height - img.rows();
		Imgproc.copyMakeBorder(img, img, ypad / 2, ypad / 2, xpad / 2,
				xpad / 2, Imgproc.BORDER_CONSTANT, new Scalar(255, 255, 255));
	}
}
