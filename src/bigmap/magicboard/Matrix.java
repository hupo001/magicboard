package bigmap.magicboard;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.utils.Converters;

public class Matrix {

	private int rows;
	private int cols;
	private String dt;
	private List<Float> data;

	public Matrix() {
	}

	public Matrix fromMat(Mat mat) {
		setRows(mat.rows());
		setCols(mat.cols());
		List<Float> fs = new ArrayList<Float>();

		for (int i = 0; i < mat.rows(); i++)
			for (int j = 0; j < mat.cols(); j++) {
				double[] ds = mat.get(i, j);
				for (double d: ds)
					fs.add(new Float(d));
			}
		setData(fs);
		setDt("f");
		return this;
	}

	public Mat toMat() {
		if (data == null || data.size() == 0) {
			return new Mat();
		}
		Mat mat = null;
		if ("f".equals(dt)) {
			mat = Converters.vector_float_to_Mat(data);
			mat = mat.reshape(mat.channels(), rows);
		} else {
			mat = new Mat();
		}
		return mat;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getCols() {
		return cols;
	}

	public void setCols(int cols) {
		this.cols = cols;
	}

	public List<Float> getData() {
		return data;
	}

	public void setData(List<Float> data) {
		this.data = data;
	}

	public String getDt() {
		return dt;
	}

	public void setDt(String dt) {
		this.dt = dt;
	}

}
