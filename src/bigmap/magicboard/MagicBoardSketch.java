package bigmap.magicboard;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.core.TermCriteria;
import org.opencv.highgui.Highgui;
import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PShape;
import processing.video.Movie;

public class MagicBoardSketch extends PApplet {
	public VideoCapture cam;

	public int numOfPostIt = 2;
	public boolean[] foundPostIt = new boolean[numOfPostIt];
	public Mat[] postItImage = new Mat[numOfPostIt];
	public Point[] postItCenter = new Point[numOfPostIt];
	public Date[] lastOccur = new Date[numOfPostIt];

	public Mat colorImage;
	public Mat image;
	public Mat grayImage;
	public Mat hsvImage;
	public Mat hueImage;
	public Mat satImage;
	public Mat valImage;
	public Mat oriImage;

	public Imshow imshow, imshow2;
	public Mat cameraMatrix;
	public Mat distCoeffs;
	public Rectangle firstMonitor;
	public Rectangle secondMonitor;
	public Rectangle arenaRect;
	public Rectangle consoleRect;
	public PImage chessboard;
	public int calibrated;
	public Map<String, PShape> shapes;
	public Map<String, PImage> images;

	public Movie movie;
	public Size boardSize;
	public int boardWidth;
	public int boardHeight;
	public List<Mat> rvecs, tvecs;
	public Mat transform;

	public double squareSize;
	public Size imageSize;
	public double arenaImageRatio;
	public double xpad;
	public Size processSize = new Size(720, 450);
	public Point movieCoordinate1;
	public Point movieCoordinate2;

	@Override
	public void init() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		setupMonitor();

		if (frame != null) {
			// trick to make it possible to change the frame properties
			frame.removeNotify();

			// comment this out to turn OS chrome back on
			frame.setUndecorated(true);

			// comment this out to not have the window "float"
			frame.setAlwaysOnTop(true);

			// frame.setResizable(true);
			frame.addNotify();

		}

		super.init();

	}

	@Override
	public boolean sketchFullScreen() {
		return false;
	}

	public static void main(String args[]) {
		PApplet.main(new String[] { "bigmap.magicboard.MagicBoardSketch" });
	}

	public void setupMonitor() {

		GraphicsEnvironment ge = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		GraphicsDevice[] gs = ge.getScreenDevices();
		// gs[1] gets the *second* screen. gs[0] would get the primary screen

		firstMonitor = gs[0].getConfigurations()[0].getBounds();
		secondMonitor = null;
		if (gs.length >= 2) {
			secondMonitor = gs[1].getConfigurations()[0].getBounds();
		}

		arenaRect = (secondMonitor != null ? secondMonitor : firstMonitor);
		consoleRect = (firstMonitor != null ? firstMonitor : secondMonitor);
		println("arena" + arenaRect);
		println("console" + consoleRect);

	}

	public void setup() {
		/* Get Shapes */
		shapes = new HashMap<String, PShape>();
		images = new HashMap<String, PImage>();

		images.put("demo", loadImage("data/demo_screens/demo-01.jpg"));
		images.put("demo2", loadImage("data/demo_screens/demo2-01.jpg"));
		images.put("michele", loadImage("data/demo_screens/michele-01.jpg"));
		images.put("ryan", loadImage("data/demo_screens/ryan-01.jpg"));

		images.put("interviews",
				loadImage("data/demo_screens/interviews-01.jpg"));
		images.put("cedric", loadImage("data/demo_screens/cedric-01.jpg"));

		shapes.put("01conference", loadShape("data/newUI/01conference.svg"));
		shapes.put("02review", loadShape("data/newUI/02review.svg"));
		shapes.put("03annualReport", loadShape("data/newUI/03annualReport.svg"));

		Map<String, Object> yamlData = YamlHelper
				.getYaml("data/intrinsics.yml");
		// cameraMatrix = ((Matrix) yamlData.get("camera_matrix")).toMat();
		// distCoeffs = ((Matrix)
		// yamlData.get("distortion_coefficients")).toMat();
		if (new File("data/transform.yml").exists()) {
			yamlData = YamlHelper.getYaml("data/transform.yml");
			transform = ((Matrix) yamlData.get("transform")).toMat();
		}

		cam = new VideoCapture(0);

		colorImage = new Mat();
		image = new Mat();
		hsvImage = new Mat();
		hueImage = new Mat();
		satImage = new Mat();
		valImage = new Mat();
		grayImage = new Mat();
		for (int i = 0; i < numOfPostIt; i++) {
			postItImage[i] = null;
			lastOccur[i] = new Date(0);
		}

		imshow = new Imshow("aa", consoleRect.width / 2,
				consoleRect.height / 2, consoleRect.x, consoleRect.y);

		imshow2 = new Imshow("aa2", consoleRect.width / 2,
				consoleRect.height / 2, consoleRect.x + consoleRect.width / 2,
				consoleRect.y);

		Mat chessboardMat = Highgui.imread("data/chessboard.png");
		ImageHelper.fit(chessboardMat, arenaRect.width, arenaRect.height);
		chessboard = Conversion.toPImage(this, chessboardMat);
		boardWidth = 25;
		boardHeight = 17;
		boardSize = new Size(boardWidth, boardHeight);

		cam.read(colorImage);
		imageSize = colorImage.size();
		squareSize = imageSize.height / (boardHeight + 3);
		xpad = imageSize.width - squareSize * (boardWidth + 1);
		arenaImageRatio = (double) arenaRect.height / processSize.height;

		size(arenaRect.width, arenaRect.height, P2D); // TODO
		background(255);

		movie = new Movie(this, "data/demo_screens/clips/clip3.mp4");

		// movie.loop();

		calibrated = 0;

		rvecs = new ArrayList<Mat>();
		tvecs = new ArrayList<Mat>();
		// start movie playing

	}

	@Override
	public void destroy() {
		if (cam != null)
			cam.release();
		super.destroy();
	}

	private void swap(double[] a, double[] b) {
		for (int i = 0; i < a.length; i++) {
			double t = a[i];
			a[i] = b[i];
			b[i] = t;
		}
	}

	public void calibrate() {
		image(chessboard, 0, 0);

		if (!(cam != null && cam.isOpened()))
			return;

		cam.read(colorImage);

		Imgproc.cvtColor(colorImage, image, Imgproc.COLOR_BGR2GRAY);
		MatOfPoint2f corners = new MatOfPoint2f();
		boolean found = Calib3d.findChessboardCorners(image, boardSize,
				corners, Calib3d.CALIB_CB_ADAPTIVE_THRESH
						| Calib3d.CALIB_CB_FAST_CHECK
						| Calib3d.CALIB_CB_NORMALIZE_IMAGE
						| Calib3d.CALIB_CB_FILTER_QUADS);
		if (!found)
			return;

		Imgproc.cornerSubPix(image, corners, new Size(11, 11),
				new Size(-1, -1), new TermCriteria(TermCriteria.EPS
						+ TermCriteria.MAX_ITER, 30, 0.1));
		Calib3d.drawChessboardCorners(colorImage, boardSize, corners, found);

		System.out.println(squareSize);
		List<Mat> objectPoints = new ArrayList<Mat>();
		List<Mat> imagePoints = new ArrayList<Mat>();

		Mat objectCorners = new Mat(corners.rows(), 1, CvType.CV_32FC3);

		for (int i = 0, k = 0; i < boardSize.height; i++)
			for (int j = 0; j < boardSize.width; j++, k++) {
				objectCorners.put(k, 0, new float[] {
						(float) (xpad / 2 + squareSize + j * squareSize),
						(float) (squareSize * 2 + i * squareSize), 0 });
			}
		objectPoints.add(objectCorners);
		imagePoints.add(corners);

		cameraMatrix = Mat.eye(3, 3, CvType.CV_32FC1);
		distCoeffs = Mat.zeros(5, 1, CvType.CV_32FC1);

		Calib3d.calibrateCamera(objectPoints, imagePoints, imageSize,
				cameraMatrix, distCoeffs, rvecs, tvecs, Calib3d.CALIB_FIX_K4
						| Calib3d.CALIB_FIX_K5);
		// Calib3d.CALIB_FIX_ASPECT_RATIO |
		Mat newCameraMatrix = Calib3d.getOptimalNewCameraMatrix(cameraMatrix,
				distCoeffs, imageSize, 0);

		Mat mapx = new Mat();
		Mat mapy = new Mat();
		Imgproc.initUndistortRectifyMap(cameraMatrix, distCoeffs, new Mat(),
				newCameraMatrix, imageSize, CvType.CV_32FC1, mapx, mapy);
		// Imgproc.remap(colorImage, colorImage, mapx, mapy,
		// Imgproc.INTER_LANCZOS4);
		// Imgproc.remap(image, image, mapx, mapy, Imgproc.INTER_LANCZOS4);

		Mat rcObjectCorners = objectCorners.reshape(3, boardHeight);
		Mat quadOriginal = new Mat();
		quadOriginal.create(4, 1, CvType.CV_32FC2);
		float[] p = new float[3];
		rcObjectCorners.get(0, 0, p);
		quadOriginal.put(0, 0, new float[] { (float) p[0], (float) p[1] });
		rcObjectCorners.get(boardHeight - 1, 0, p);
		quadOriginal.put(1, 0, new float[] { (float) p[0], (float) p[1] });
		rcObjectCorners.get(0, boardWidth - 1, p);
		quadOriginal.put(2, 0, new float[] { (float) p[0], (float) p[1] });
		rcObjectCorners.get(boardHeight - 1, boardWidth - 1, p);
		quadOriginal.put(3, 0, new float[] { (float) p[0], (float) p[1] });

		Mat rcCorners = corners.reshape(2, boardHeight);
		Mat quad = new Mat();
		quad.create(4, 1, CvType.CV_32FC2);
		double tl[] = rcCorners.get(0, 0);
		double bl[] = rcCorners.get(boardHeight - 1, 0);
		double tr[] = rcCorners.get(0, boardWidth - 1);
		double br[] = rcCorners.get(boardHeight - 1, boardWidth - 1);

		if (tl[0] > tr[0]) {
			swap(tr, tl);
			swap(bl, br);
		}
		if (tl[1] > bl[1]) {
			swap(bl, tl);
			swap(br, tr);
		}

		quad.put(0, 0, tl);
		quad.put(1, 0, bl);
		quad.put(2, 0, tr);
		quad.put(3, 0, br);
		Mat colorImage2 = colorImage.clone();
		Core.circle(colorImage2, new Point(tl), 10, new Scalar(255, 0, 0));
		Core.circle(colorImage2, new Point(bl), 10, new Scalar(255, 0, 0));
		Core.circle(colorImage2, new Point(tr), 10, new Scalar(255, 0, 0));
		Core.circle(colorImage2, new Point(br), 10, new Scalar(255, 0, 0));

		transform = Imgproc.getPerspectiveTransform(quad, quadOriginal);

		imshow.showImage(colorImage2);

		Imgproc.warpPerspective(colorImage, colorImage, transform, imageSize,
				Imgproc.INTER_LANCZOS4);
		double pp[] = rcObjectCorners.get(boardHeight - 1, boardWidth - 1);
		Core.circle(colorImage, new Point(pp), 10, new Scalar(255, 0, 0));
		pp = rcObjectCorners.get(boardHeight - 1, 0);
		Core.circle(colorImage, new Point(pp), 10, new Scalar(255, 0, 0));
		imshow2.showImage(colorImage);

		Map<String, Object> yamlData = new HashMap<String, Object>();

		yamlData.put("transform", (Object) new Matrix().fromMat(transform));
		yamlData.put("cameraMatrix",
				(Object) new Matrix().fromMat(cameraMatrix));
		yamlData.put("distCoeffs", (Object) new Matrix().fromMat(distCoeffs));
		yamlData.put("quadOriginal",
				(Object) new Matrix().fromMat(quadOriginal));
		yamlData.put("quad", (Object) new Matrix().fromMat(quad));

		YamlHelper.saveYaml("data/transform.yml", yamlData);

		// PImage pimage = new PImage();
		// Conversion.toPImage(this, colorImage, pimage);
		calibrated++;
	}

	public double getAreaRatio(Mat image, int lower, int upper,
			List<MatOfPoint> contours, int contourIdx) {
		MatOfPoint contour = contours.get(contourIdx);
		Rect rect = Imgproc.boundingRect(contour);
		Mat sub = new Mat();
		Core.inRange(image.submat(rect), new Scalar(lower), new Scalar(upper),
				sub);
		Mat mask = Mat.zeros(colorImage.size(), CvType.CV_8U);
		Imgproc.drawContours(mask, contours, contourIdx, new Scalar(255), -1);
		Mat subMask = mask.submat(rect);
		Mat crop = Mat.zeros(subMask.size(), CvType.CV_8U);
		sub.copyTo(crop, subMask);
		int cnt = Core.countNonZero(crop);
		double area = Core.countNonZero(subMask);
		return (double) cnt / area;
	}

	public Point getRealCenter(Point center) {
		double xmargin = (xpad / 2 - 2 * squareSize) / imageSize.height
				* processSize.height;
		return new Point(((center.x - xmargin) * arenaImageRatio),
				(center.y * arenaImageRatio));
	}

	public void moveToCenter(Point center) {
		double xmargin = (xpad / 2 - 2 * squareSize) / imageSize.height
				* processSize.height;
		translate((float) ((center.x - xmargin) * arenaImageRatio),
				(float) (center.y * arenaImageRatio));
	}

	public boolean process(List<MatOfPoint> contours, int contourIdx) {
		MatOfPoint contour = contours.get(contourIdx);
		double area = Imgproc.contourArea(contour);
		if (contour.rows() < 3 || contour.rows() > 4 || area < 50 || area > 400
				|| !Imgproc.isContourConvex(contour))
			return false;
		double expectedAngle = Math.PI * (contour.rows() - 2) / contour.rows();

		Point[] points = contour.toArray();
		for (int i = 0; i < contour.rows(); i++) {
			Point p1 = points[(i - 1 + contour.rows()) % contour.rows()];
			Point p2 = points[i];
			Point p3 = points[(i + 1) % contour.rows()];
			Point v1 = new Point(p1.x - p2.x, p1.y - p2.y);
			Point v2 = new Point(p3.x - p2.x, p3.y - p2.y);
			double angle = Math.acos(v1.dot(v2)
					/ Math.sqrt(v1.dot(v1) * v2.dot(v2)));
			if (Math.abs(angle - expectedAngle) / expectedAngle > 0.4) {
				return false;
			}
			if (Math.abs(Math.sqrt(v1.dot(v1)) - Math.sqrt(v2.dot(v2)))
					/ Math.sqrt(v1.dot(v1)) > 0.4) {
				return false;
			}
		}

		Moments mu = Imgproc.moments(contour);
		Point center = new Point(mu.get_m10() / mu.get_m00(), mu.get_m01()
				/ mu.get_m00());

		// double hueRatio = getAreaRatio(hueImage, 50, 70, contours,
		// contourIdx);

		
		if (contour.rows() == 3
				&& getAreaRatio(satImage, 150, 230, contours, contourIdx) > 0.5) { // Green
			movieCoordinate2 = getRealCenter(center);

			return true;
		}
		if (contour.rows() == 3
				&& getAreaRatio(hueImage, 90, 130, contours, contourIdx) > 0.7) { // Blue
																					// 3
			postItCenter[1] = new Point(center.x, center.y + 50);
//return false;
			return true;
		}

		if (contour.rows() == 3
				&& getAreaRatio(hueImage, 0, 30, contours, contourIdx) > 0.7) { // Red
			//postItCenter[1] = new Point(center.x, center.y + 50);
													// 3
			return false;
			//return false;
//			return true;
			//
			// pushMatrix();
			// moveToCenter(center);
			// shape(shapes.get("01conference"), 7, -100);
			// popMatrix();
			//
			// pushMatrix();
			// moveToCenter(center);
			// shape(shapes.get("02review"), 0, 0);
			// popMatrix();
			//
			// pushMatrix();
			// moveToCenter(center);
			// shape(shapes.get("03annualReport"), 0, 120);
			// popMatrix();
			//
			// return true;
		}

		if (contour.rows() == 3
				&& getAreaRatio(valImage, 0, 100, contours, contourIdx) > 0.7) { // Black
																					// Triangle

			movieCoordinate1 = getRealCenter(center);

			return true;
		}
		if (contour.rows() == 4
				&& getAreaRatio(hueImage, 100, 130, contours, contourIdx) > 0.7) { // Blue
																					// 4

			showLeftImages(center);
			return true;
		}
		if (contour.rows() == 4
				&& getAreaRatio(valImage, 0, 100, contours, contourIdx) > 0.7) { // Black
																					// 4

			showRightImages(center);
			return true;
		}
		

		return false;
	}

	public void showRightImages(Point center) {
		pushMatrix();
		//
		moveToCenter(center);

		image(images.get("demo"), -300, -150, 234, 133);
		image(images.get("michele"), -300, 0, 234, 133);
		image(images.get("ryan"), -300, 150, 234, 133);
		popMatrix();
	}

	public void showLeftImages(Point center) {
		pushMatrix();
		moveToCenter(center);

		image(images.get("cedric"), 50, -150, 234, 133);
		image(images.get("interviews"), 50, 0, 234, 133);
		image(images.get("demo2"), 50, 150, 234, 133);

		popMatrix();

	}

	public boolean processPostIt(List<MatOfPoint> contours, int contourIdx) {
		MatOfPoint contour = contours.get(contourIdx);
		if (contour.rows() != 6 || !Imgproc.isContourConvex(contour))
			return false;
		double area = Imgproc.contourArea(contour);

		System.out.println(contour.rows() + " " + area);
		if (area < 3000 || area > 4000)
			return false;
		double expectedAngle = Math.PI * (contour.rows() - 2) / contour.rows();

		Point[] points = contour.toArray();
		for (int i = 0; i < contour.rows(); i++) {
			Point p1 = points[(i - 1 + contour.rows()) % contour.rows()];
			Point p2 = points[i];
			Point p3 = points[(i + 1) % contour.rows()];
			Point v1 = new Point(p1.x - p2.x, p1.y - p2.y);
			Point v2 = new Point(p3.x - p2.x, p3.y - p2.y);
			double angle = Math.acos(v1.dot(v2))
					/ Math.sqrt(v1.dot(v1) * v2.dot(v2));
			if (Math.abs(angle - expectedAngle) / expectedAngle > 0.4) {
				return false;
			}
			if (Math.abs(Math.sqrt(v1.dot(v1)) - Math.sqrt(v2.dot(v2)))
					/ Math.sqrt(v1.dot(v1)) > 0.4) {
				return false;
			}
		}

		Moments mu = Imgproc.moments(contour);
		Point center = new Point(mu.get_m10() / mu.get_m00(), mu.get_m01()
				/ mu.get_m00());
		//

		Rect rect = Imgproc.boundingRect(contour);
		Mat sub = colorImage.submat(rect);
		Mat mask = Mat.zeros(colorImage.size(), CvType.CV_8U);
		Imgproc.drawContours(mask, contours, contourIdx, new Scalar(255), -1);
		Mat subMask = mask.submat(rect);
		Mat crop = Mat.zeros(subMask.size(), sub.type());
		crop.setTo(new Scalar(255, 255, 255));

		sub.copyTo(crop, subMask);
		Imgproc.resize(crop, crop, new Size(0, 0), arenaImageRatio,
				arenaImageRatio, Imgproc.INTER_LANCZOS4);
		Highgui.imwrite("data/hueTest.png", hueImage);
		int postItIdx = -1;
	/*	if (getAreaRatio(hueImage, 0, 30, contours, contourIdx) > 0.7)
			postItIdx = 1;
		else
			postItIdx = 0;
*/
	//	if (postItIdx >= 0) {
		postItIdx = 1;
			postItImage[postItIdx] = crop.clone();
			lastOccur[postItIdx] = new Date();
			postItCenter[postItIdx] = center;
			foundPostIt[postItIdx] = true;
			return true;
	//	}
		//return false;
	}

	@Override
	public void draw() {
		frame.setLocation(arenaRect.x, arenaRect.y);

		if (calibrated < 0) {
			calibrate();
			return;
		}

		background(255);

		if (cam != null && cam.isOpened()) {

			cam.read(colorImage);
			// Highgui.imwrite("data/screen_of_tag.png", colorImage);
			// colorImage = Highgui.imread("data/screen_of_tag.png");

			Imgproc.warpPerspective(colorImage, colorImage, transform,
					colorImage.size(), Imgproc.INTER_LINEAR);
			oriImage = colorImage.clone();
			Imgproc.resize(colorImage, colorImage, processSize);
			imshow.showImage(colorImage);

			Imgproc.cvtColor(colorImage, hsvImage, Imgproc.COLOR_BGR2HSV);
			List<Mat> hsvChannels = new ArrayList<Mat>();
			Core.split(hsvImage, hsvChannels);
			hueImage = hsvChannels.get(0);
			satImage = hsvChannels.get(1);
			valImage = hsvChannels.get(2);
			Imgproc.cvtColor(colorImage, grayImage, Imgproc.COLOR_BGR2GRAY);

			List<MatOfPoint> finalContourMats = new ArrayList<MatOfPoint>();
			//
			// Highgui.imwrite("data/hue.png", hueImage);
			// Highgui.imwrite("data/sat.png", satImage);
			// Highgui.imwrite("data/val.png", valImage);
			// Highgui.imwrite("data/gray.png", grayImage);

			image = grayImage.clone();
			Imgproc.blur(image, image, new Size(1, 1));
			Imgproc.threshold(image, image, 150, 255, Imgproc.THRESH_BINARY);
			Highgui.imwrite("data/gray_thr.png", image);

			ArrayList<MatOfPoint> contourMats = new ArrayList<MatOfPoint>();
			Mat hierarchy = new Mat();
			Imgproc.findContours(image, contourMats, hierarchy,
					Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

			int i = 0;
			movieCoordinate1 = movieCoordinate2 = null;
			for (MatOfPoint contour : contourMats) {
				MatOfPoint2f coutour2f = new MatOfPoint2f();
				MatOfPoint2f approx2f = new MatOfPoint2f();
				contour.convertTo(coutour2f, CvType.CV_32FC2);

				Imgproc.approxPolyDP(coutour2f, approx2f,
						image.size().height * 0.01, true);
				approx2f.convertTo(contour, CvType.CV_32S);

				if (process(contourMats, i))
					finalContourMats.add(contour);
				i++;
			}

			image = satImage.clone();
			Imgproc.blur(image, image, new Size(1, 1));
			Highgui.imwrite("data/sat.png", image);

			Imgproc.threshold(image, image, 100, 255, Imgproc.THRESH_BINARY);
			Highgui.imwrite("data/sat_thr.png", image);

			contourMats = new ArrayList<MatOfPoint>();
			Imgproc.findContours(image, contourMats, hierarchy,
					Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

			for (int j = 0; j < numOfPostIt; j++)
				foundPostIt[j] = false;

			i = 0;
			for (MatOfPoint contour : contourMats) {
				MatOfPoint2f coutour2f = new MatOfPoint2f();
				MatOfPoint2f approx2f = new MatOfPoint2f();
				contour.convertTo(coutour2f, CvType.CV_32FC2);

				Imgproc.approxPolyDP(coutour2f, approx2f,
						image.size().height * 0.01, true);
				approx2f.convertTo(contour, CvType.CV_32S);

				if (processPostIt(contourMats, i)) {
					finalContourMats.add(contour);
				}
				i++;
			}

			if (movieCoordinate1 != null) {
				movie.play();
				movie.read();
				movie.loop();
				float pad = 50;
				if (movieCoordinate2 != null) {

					double scale = Math
							.min((Math.abs(movieCoordinate1.x
									- movieCoordinate2.x) - pad)
									/ movie.width,
									(Math.abs(movieCoordinate1.y
											- movieCoordinate2.y) - pad)
											/ movie.height);

					image(movie,
							(float) Math.min(movieCoordinate1.x,
									movieCoordinate2.x) + pad / 2,
							(float) Math.min(movieCoordinate1.y,
									movieCoordinate2.y) + pad / 2,
							(float) (movie.width * scale),
							(float) (movie.height * scale));

				} else {

					double expectHeight = 133;
					double expectWidth = (double) movie.width / movie.height
							* expectHeight;
					image(movie,
							(float) (movieCoordinate1.x - expectWidth - pad),
							(float) (movieCoordinate1.y - expectHeight - pad),
							(float) expectWidth, (float) expectHeight);

				}

				// image(Conversion.toPImage(this, subColor), 0, 0);

			} else {
				// movie.stop();
				// movie.noLoop();
			}

			for (int j = 0; j < numOfPostIt; j++)
				if (!foundPostIt[j] && postItImage[j] != null
						&& new Date().getTime() - lastOccur[j].getTime() > 1000) {
					PImage pimage = Conversion.toPImage(this, postItImage[j]);

					pushMatrix();
					moveToCenter(postItCenter[j]);

					Size postItSize = postItImage[j].size();

					image(pimage, -(float) postItSize.width / 2,
							-(float) postItSize.height / 2);
					fill(255, 255, 0, 255);
					noStroke();
					rect(-(float) postItSize.width / 2 + 25,
							-(float) postItSize.height / 2 + 10, 50, -20);
					popMatrix();
				}

			Imgproc.drawContours(colorImage, finalContourMats, -1, new Scalar(
					0, 255, 0), 1);

			imshow.showImage(colorImage);
			// Highgui.imwrite("data/gray_thr2.png", colorImage);

			// PImage pimage = new PImage();
			// Conversion.toPImage(this, colorImage, pimage);

		}

	}
}
