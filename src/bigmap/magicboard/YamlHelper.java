package bigmap.magicboard;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;

public class YamlHelper {
	
	public static Yaml yaml;
	public static Constructor matrixConstructor;
	static {
		matrixConstructor = new Constructor() {
			protected Class<?> getClassForName(String name)
					throws ClassNotFoundException {
				if ("opencv-matrix".equals(name)) {
					return Matrix.class;
				}
				return super.getClassForName(name);
			}
		};

		Representer representer = new Representer();
		representer.addClassTag(Matrix.class, new Tag("!!opencv-matrix"));
		yaml = new Yaml(matrixConstructor, representer);

	}

	public static void saveYaml(String filename, Object data) {
		FileWriter bos = null;
		try {
			bos = new FileWriter(filename);
			yaml.dump(data, bos);
			bos.close();
		} catch (IOException e) {
			System.err.println(e);
		}
	}

	public static Map<String, Object> getYaml(String filename) {
		BufferedInputStream bis = null;
		try {
			bis = new BufferedInputStream(new FileInputStream(filename));
		} catch (FileNotFoundException e) {
			System.err.println(e);
		}

		Map<String, Object> yamlData = (Map<String, Object>) yaml.load(bis);
		return yamlData;
	}

}
